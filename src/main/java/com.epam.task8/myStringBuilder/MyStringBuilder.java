package com.epam.task8.myStringBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MyStringBuilder {
  private List<String> strings = new ArrayList<String>();

  public void append(String str) {
    strings.add(str);
  }

  public void append(char ch) {
    strings.add(String.valueOf(ch));
  }

  public void append(char[] chars) {
    strings.add(String.valueOf(chars));
  }

  public void append(int i) {
    strings.add(String.valueOf(i));
  }

  public void append(boolean bool) {
    strings.add(String.valueOf(bool));
  }

  public String concat() {
    return strings.stream().collect(Collectors.joining(""));
  }
}
