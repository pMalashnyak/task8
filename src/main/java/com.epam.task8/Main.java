package com.epam.task8;

import com.epam.task8.RegularExpresion.Checker;
import com.epam.task8.myStringBuilder.MyStringBuilder;

public class Main {
  public static void main(String[] args) {
    MyStringBuilder sb = new MyStringBuilder();
    sb.append(23);
    sb.append("AAAA");
    sb.append('q');

    boolean b = true;
    sb.append(b);

    char[] chars = {'a', 'b', 'c'};

    sb.append(chars);

    System.out.println(sb.concat() + " ");

    String check1 = "Text to check.";
    String check2 = "incorrect text";
    System.out.println(
        "Check "
            + check1
            + ": "
            + Checker.check(check1)
            + "\nCheck "
            + check2
            + ": "
            + Checker.check(check2));

    String theyou = "the you you hh r thethe";
    System.out.println(Checker.split(theyou));

    String vowels = "Sample text.";
    System.out.println("Start: " + vowels + " Replased: " + Checker.replaseVowels(vowels));
  }
}
